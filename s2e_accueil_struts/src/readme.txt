Nom du projet : accueil-struts
Techno : struts/JDBC
Pour qu'il fonctionne chez vous, il faut mettre vos données personnelles dans :
- la classe com.greta.accueilstruts.globals.Constantes
- le fichier de configuration de struts : WedContent/WEB-INF/struts-config.xml

! Ne pas oublier de mettre le driver JDBC : mysql-connector-java-5.1.40-bin.jar dans 
le dossier lib de Tomcat (C:\Program Files\Apache Software Foundation\Tomcat 8.0\lib)

Bibliothèques à mettre dans /WEB-INF/lib/ :
antlr-2.7.2.jar
bsf-2.3.0.jar
commons-beanutils-1.8.0.jar
commons-chain-1.2.jar
commons-digester-1.8.jar
commons-fileupload-1.1.1.jar
commons-io-1.1.jar
commons-logging-1.0.4.jar
commons-validator-1.3.1.jar
jstl-1.0.2.jar
log4j-1.2.17.jar
oro-2.0.8.jar
standard-1.0.6.jar
struts-core-1.3.10.jar
struts-el-1.3.10.jar
struts-extras-1.3.10.jar
struts-faces-1.3.10.jar
struts-mailreader-dao-1.3.10.jar
struts-scripting-1.3.10.jar
struts-taglib-1.3.10.jar
struts-tiles-1.3.10.jar

! Pensez à encrypter vos mots de passe dans votre base de données. 
La comparaison des mdp se fait avec des mdp encryptés en sha1.