package com.greta.s2e_accueil_struts.dao;

import com.greta.s2e_accueil_struts.beans.Utilisateur;

public interface UtilisateurDao {
	public Utilisateur getElementByLoginPassword(String login, String password);
}
