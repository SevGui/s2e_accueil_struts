package com.greta.s2e_accueil_struts.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.greta.s2e_accueil_struts.beans.Utilisateur;

import static com.greta.s2e_accueil_struts.globals.Constantes.*;

public class UtilisateurDaoImpl implements UtilisateurDao {

	private static final Logger LOGGER = Logger.getLogger(UtilisateurDaoImpl.class);

	private Connection connection = null;

	/**
	 * getElementByLoginPassword : récupère les données d'un utilisateur en base 
	 * de données à partir de son login et de son mot de passe
	 * @param String login
	 * @param String password
	 * @return Utilisateur : objet entity contenant les données
	 * constantes définies dans la classe com.greta.accueilstruts.globals.Constantes
	 */
	@Override
	public Utilisateur getElementByLoginPassword(String login, String password) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Utilisateur utilisateur = null;
		
		//chargement du driver
		sqlLoadDriver();
		
		try {
			//connexion à la BDD
			connection = DriverManager.getConnection(BDD_URL, BDD_USER, BDD_PSW);
			LOGGER.info("Connexion réussie");

			// Préparation de la requête
			// le mot de passe est encrypté dans la requête pour être comparé avec celui, 
			//encrypté, de la BDD
			if (null != connection) {
				preparedStatement = connection.prepareStatement(
						"SELECT * FROM " + TABLE_UTILISATEUR + " WHERE "
						+ TABLE_UTILISATEUR_LOGIN + "=? AND " + TABLE_UTILISATEUR_PASSWORD 
						+ "=sha1(?);");
				preparedStatement.setString(1, login);
				preparedStatement.setString(2, password);
			} else {
				LOGGER.fatal("Connexion null");
			}

			// Execution de la requête
			try {
				resultSet = preparedStatement.executeQuery();
			} catch (SQLException exception) {
				LOGGER.fatal("Erreur requête : " + exception);
			}
			
			// Mappage du ResultSet en Utilisateur
			if (resultSet.next()) {
				utilisateur = map(resultSet);
			}

		} catch (SQLException exception) {
			LOGGER.fatal("Erreur connexion : " + exception);
		} finally {
			//fermeture de la connexion
			if (null != connection) {
				try {
					LOGGER.info("Fermeture de la connexion");
					connection.close();
					LOGGER.info("Connexion fermée");
				} catch (SQLException ignore) {
					LOGGER.debug("Erreur ignorée à la fermeture : " + ignore);
				}
			}
		}
		
		return utilisateur;
	}

	/**
	 * sqlLoadDriver : chargement du driver JDBC
	 * constantes définies dans la classe com.greta.accueilstruts.globals.Constantes
	 */
	private void sqlLoadDriver() {
		// Chargement du driver JDBC Oracle
		// ne pas oublier de mettre le jar correspondant dans le dossier lib de Tomcat
		try {
			Class.forName(MYSQL_DRIVER);
			LOGGER.info("Driver OK");
		} catch (ClassNotFoundException exception) {
			LOGGER.fatal("Impossible de charger le driver : " + exception);
		}
	}

	/**
	 * map : parse les données d'un ResulSet en Utilisateur
	 * @param ResultSet resultSet : données de retour de la requête en BDD
	 * @return Utilisateur : objet entity contenant les données
	 * constantes définies dans la classe com.greta.accueilstruts.globals.Constantes
	 */
	private Utilisateur map(ResultSet resultSet) throws SQLException {
		Utilisateur element = new Utilisateur(
				resultSet.getInt(TABLE_UTILISATEUR_ID),
				resultSet.getString(TABLE_UTILISATEUR_IDROLE), 
				resultSet.getString(TABLE_UTILISATEUR_NOM),
				resultSet.getString(TABLE_UTILISATEUR_PRENOM),
				resultSet.getInt(TABLE_UTILISATEUR_IDETABLISSEMENT),
				resultSet.getString(TABLE_UTILISATEUR_LOGIN), 
				resultSet.getString(TABLE_UTILISATEUR_PASSWORD));
		return element;
	}
	
}
