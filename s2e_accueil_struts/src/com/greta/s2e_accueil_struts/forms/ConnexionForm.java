package com.greta.s2e_accueil_struts.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * Classe d'affichage du formulaire
 */
public class ConnexionForm extends ActionForm {
	private static final long serialVersionUID = 1L;

	private String login;
	private String password;
	
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		// contrôle de la saisie des champs -> messages paramétrés dans
		// src/resource/MessageResource.properties
		ActionErrors actionErrors = new ActionErrors();
		if (login == null || login.trim().equals("")) {
			actionErrors.add("login", new ActionMessage("error.login"));
		}
		if (password == null || password.trim().equals("")) {
			actionErrors.add("password", new ActionMessage("error.password"));
		}
		return actionErrors;
	}
	
}
