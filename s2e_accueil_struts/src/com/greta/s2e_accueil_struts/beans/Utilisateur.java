package com.greta.s2e_accueil_struts.beans;

/**
 * Classe entity Utilisateur (les ojets liés ne sont pas nécéssaires donc pa implémentés (role, etablissement...)
 */
public class Utilisateur {
	
	private int id;
	private String idRole;
	private String nom;
	private String prenom;
	private int idEtablissement;
	private String login;
	private String password;
	
	public Utilisateur() { }

	public Utilisateur(String nom, String idRole, String prenom, int idEtablissement, String login, String password) {
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.password = password;
		this.idRole = idRole;
		this.idEtablissement = idEtablissement;
	}

	public Utilisateur(int id, String idRole, String nom, String prenom, int idEtablissement, String login, String password) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.password = password;
		this.idRole = idRole;
		this.idEtablissement = idEtablissement;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIdRole() {
		return idRole;
	}

	public void setIdRole(String idRole) {
		this.idRole = idRole;
	}

	public int getIdEtablissement() {
		return idEtablissement;
	}

	public void setIdEtablissement(int idEtablissement) {
		this.idEtablissement = idEtablissement;
	}
	
}
