package com.greta.s2e_accueil_struts.actions;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.greta.s2e_accueil_struts.beans.Utilisateur;
import com.greta.s2e_accueil_struts.dao.UtilisateurDao;
import com.greta.s2e_accueil_struts.dao.UtilisateurDaoImpl;
import com.greta.s2e_accueil_struts.forms.ConnexionForm;

import static com.greta.s2e_accueil_struts.globals.Constantes.*;

/**
 * Classe de traitement du formulaire
 */
public class ConnexionAction extends Action{

	private static final Logger LOGGER = Logger.getLogger(ConnexionAction.class);
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		request.setCharacterEncoding("UTF-8");
		
		Utilisateur utilisateur = null;
		String target = null;
		String message = "";
		
		//récupération du formulaire
		ConnexionForm loginForm = (ConnexionForm) form;
		//récupération des données de l'utilisateur à  partir de son login et de son mot de passe
		utilisateur = getElementByLoginPassword(loginForm.getLogin(), loginForm.getPassword());
		
		if (null != utilisateur) {
			//récupération du site vers lequel rediriger en fonction du role 
			//-> à  configurer dans WedContent/WEB-INF/struts-config.xml
			target = redirect(utilisateur.getIdRole());			
			message = utilisateur.getNom();
			
			//Création du cookie pour transmettre l'information de l'utilisateur connecté aux autres sites du projet
			Cookie cookie = new Cookie(COOKIE_S2E, String.valueOf(utilisateur.getId()));
			//changement de la durée de vie du cookie pour qu'il dure plus longtemps que la session en cours
			cookie.setMaxAge(1*24*60*60*1000);
			//changement du path pour permettre à  tous les sites de lire le cookie
			cookie.setPath(COOKIE_S2E_PATH);
			response.addCookie(cookie);
			
			LOGGER.info("Connexion : création du cookie");
			
		} else {
			
			message = "Utilisateur inconnu";
			//redirection vers la page d'erreur 
			//-> à  configurer dans WedContent/WEB-INF/struts-config.xml
			target = "failure";
			
			//destruction du cookie
			Cookie cookie = new Cookie(COOKIE_S2E,"");
			cookie.setPath(COOKIE_S2E_PATH);
			cookie.setMaxAge(-1);
			response.addCookie(cookie);
			
			LOGGER.info("Echec connexion : destruction du cookie");
		}
		
		request.setAttribute("message", message);
		//redirection
		return mapping.findForward(target);
		
	}


	/**
	 * getElementByLoginPassword : récupère les informations de l'utilisateur en BDD en fonction du login et du password
	 * @param String login : login saisi
	 * @param Strign password : password saisi
	 * @return Utilisateur : objet contenant les données an BDD ou null si rien ne correspond au login/password
	 */
	private Utilisateur getElementByLoginPassword(String login, String password) {
		UtilisateurDao dao = new UtilisateurDaoImpl();
		return dao.getElementByLoginPassword(login, password);
	}
	
	/**
	 * redirect : en fonction du role, renvoie le "forward" vers lequel rediriger (défini dans WedContent/WEB-INF/struts-config.xml)
	 * @param String role : identifiant du role
	 * @return String : name du "forward" vers lequel rediriger
	 * constantes définies dans la classe com.greta.s2e_accueil_struts.globals.Constantes
	 */
	private String redirect(String role) {
		String redirection;
		switch (role) {
			case DROIT_ADMINISTRATEUR :
				redirection = SITE_ADMINISTRATEUR;
				break;
			case DROIT_ETABLISSEMENT :
				redirection = SITE_ETABLISSEMENT;
				break;
			case DROIT_MAGASINIER :
				redirection = SITE_MAGASINIER;
				break;
			case DROIT_SIEGE :
				redirection = SITE_SIEGE;
				break;
	
			default:
				redirection = "success";
				break;
			}
		return redirection;
	}
}
