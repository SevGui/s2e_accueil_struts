package com.greta.s2e_accueil_struts.globals;

public class Constantes {
	
	/* Paramètres du cookie qui contiendra l'id de l'utilisateur connecté */
	public static final String COOKIE_S2E = "s2e_user";
	public static final String COOKIE_S2E_PATH = "/";
	
	/* Droits */
	public static final String DROIT_ADMINISTRATEUR = "AM";
	public static final String DROIT_ETABLISSEMENT = "ET";
	public static final String DROIT_MAGASINIER = "MG";
	public static final String DROIT_SIEGE = "SG";
	
	/* Redirections = name des forward définis dans WedContent/WEB-INF/struts-config.xml */
	public static final String SITE_ADMINISTRATEUR = "success_administrateur";
	public static final String SITE_ETABLISSEMENT = "success_etablissement";
	public static final String SITE_MAGASINIER = "success_magasinier";
	public static final String SITE_SIEGE = "success_siege";

	/* Paramètres de connexion à la base de données */
	public static final String BDD_URL = "jdbc:mysql://127.0.0.1:3306/s2e_test";
	public static final String BDD_USER = "severine";
	public static final String BDD_PSW = "azerty";
	public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	
	/* Table Utilisateur*/
	public static final String TABLE_UTILISATEUR = "utilisateurs";
	public static final String TABLE_UTILISATEUR_ID = "utlId";
	public static final String TABLE_UTILISATEUR_IDROLE = "utlIdRole";
	public static final String TABLE_UTILISATEUR_NOM = "utlNom";
	public static final String TABLE_UTILISATEUR_PRENOM = "utlPrenom";
	public static final String TABLE_UTILISATEUR_IDETABLISSEMENT = "utlIdEtab";
	public static final String TABLE_UTILISATEUR_LOGIN = "utlLogin";
	public static final String TABLE_UTILISATEUR_PASSWORD = "utlMdp";
}
