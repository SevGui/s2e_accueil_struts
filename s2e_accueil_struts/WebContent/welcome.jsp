<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/CSS" href="bootstrap/css/bootstrap.css">
<title>Welcome page</title>
</head>
<body class="theme-invert">
	<div class="container">
	
			<div class="row">
				<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 text-center">
	<%
		String message = (String) request.getAttribute("message");
	%>
	<h1><bean:message key="label.welcome" /><%= message %></h1>
				</div>
			</div>
	</div>
	
</body>
</html>