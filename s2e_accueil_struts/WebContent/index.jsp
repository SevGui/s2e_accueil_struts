<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html"%>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/CSS"
	href="bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/styles.css" />
<title>S2E</title>
</head>
<body>
	<div class="container-fluid">
		<header class="row">
		<div class="header-wrap col-xs-12">
			<img class="col-xs-offset-1 col-xs-2" src="images/logo07-2.png"
				alt="" />
			<div class="col-xs-offset-3 col-xs-1">
				<h3>Login</h3>
				<%
					String message = (String) request.getAttribute("message");
					if (null != message && "" != message) {
						out.print(message);
					}
				%>
			</div>
			<div class="col-xs-4 ">
				<html:form action="connexion">
					<div class="form-group col-sm-12">
						<label class="control-label col-sm-4" for="login"> <bean:message
								key="label.login" />
						</label>
						<div class="col-sm-8 login">
							<html:text property="login"></html:text>
							<html:errors property="login" />
						</div>
					</div>
					<div class="form-group col-sm-12">
						<label class="control-label col-sm-4" for="password"> <bean:message
								key="label.password" />
						</label>
						<div class="col-sm-8 password">
							<html:password property="password"></html:password>
							<html:errors property="password" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8">
							<button id="valide" name="valide" class="btn btn-default btn-sm">Valider</button>
						</div>
					</div>
				</html:form>
			</div>
		</div>
		</header>
		<nav class="mainmenu">
		<div class="container">
			<ul class="nav nav-pills col-sm-offset-1 col-sm-4">
				<li class="active"><a href="index.jsp" class="active">Home</a></li>
				<li><a href="#">About</a></li>
				<li><a href="#">Services</a></li>
				<li><a href="#">contact</a></li>
			</ul>
			<ul class="nav nav-pills col-sm-offset-3 col-sm-4">
				<li><a class="social" href="#"><img src="images/aim.png"
						alt="" /></a></li>
				<li><a class="social" href="#"><img
						src="images/facebook.png" alt="" /></a></li>
				<li><a href="#"><img src="images/twwtter.png" alt="" /></a></li>
				<li><a href="#"><img src="images/linkedin.png" alt="" /></a></li>
			</ul>
		</div>
		</nav>
		<main class="container">
		<div class="row banner-wrap">
			<div class="banner col-sm-12">
				<div class="banner-img">
					<img src="images/image1.png" alt="" />
				</div>
			</div>
			<div class="shadow"></div>
		</div>
		<div class="row wrap">
			<div class="col-sm-offset-2">
				<div class="box col-sm-4">
					<div class="panel">
						<div class="title">
							<span><img src="images/icon1.png" alt="icon" /></span>
							<h1>Lorem ipsum</h1>
						</div>
						<div class="content">
							<p>Sed ut perspiciatis unde omnis iste natus error sit
								voluptatem accusantium dolo remque la udantium totam consequat.</p>
							<div class="button marTop30">
								<a href="#">More Info</a>
							</div>
						</div>
					</div>
				</div>

				<div class="box col-sm-4">
					<div class="panel">
						<div class="title">
							<span><img src="images/icon2.png" alt="icon" /></span>
							<h1>Dapibus plac</h1>
						</div>
						<div class="content">
							<p>Curabitur in nisl tortor a hendrerit magna. Fusce
								lobortis, massa ut aliquet viverra dolor sem convallis odio, non
								ornare.</p>
							<div class="button marTop30">
								<a href="#">More Info</a>
							</div>
						</div>
					</div>
				</div>

				<div class="box col-sm-4">
					<div class="panel">
						<div class="title">
							<span><img src="images/icon3.png" alt="icon" /></span>
							<h1>Lorem ipsum</h1>
						</div>
						<div class="content">
							<p>Mauris ornare eros in purus cursus mollis. Etiam nec
								gravida magna. Phasellus tincidunt tortor at magna porta sodales
								orbi ut.</p>
							<div class="button marTop30">
								<a href="#">More Info</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</main>
		<footer class="container footer-wrap">
		<div class="row">
			<div class="wrap">
				<div class="col-sm-offset-1 col-sm-4">
					<div class="title">
						<h1>Testimoniala</h1>
					</div>
					<div class="content">
						<div class="testimonials">Accusantium dolo remque la
							udantium totam consequat tortor pretium sit amet volup</div>
					</div>
				</div>
				<div class="col-sm-offset-3 col-sm-3">
					<div class="title">
						<h1>Contact us</h1>
					</div>
					<div class="cotact">
						<ul>
							<li><img src="images/icon6.png" alt="" />(000) 888 888888</li>
							<li><img src="images/icon7.png" alt="" /><a href="#">info@companyname.com</a></li>
							<li><img src="images/icon8.png" alt="" />Name of site owner</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		</footer>
	</div>
</body>
</html>